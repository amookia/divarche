package main

import (
	"log"
	"net"

	"gitlab.com/amookia/divarche/users/internal/users"
	"gitlab.com/amookia/divarche/users/pkg/mongodb"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	//connect to mongodb
	db, err := mongodb.MongoInit("divarche", "mongodb://127.0.0.1:27017")
	if err != nil {
		log.Default().Println(err)
	}
	var port = "9092"
	gs := grpc.NewServer()
	logger, _ := zap.NewProduction()

	defer logger.Sync()

	nu := users.NewUsers(users.NewRepository(db, logger), logger)

	users.RegisterUsersServer(gs, nu)
	reflection.Register(gs)

	logger.Sugar().Infoln("GRPC (users) service started at port : ", port)
	l, _ := net.Listen("tcp", ":"+port)
	gs.Serve(l)

}
