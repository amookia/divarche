package users

import (
	"context"
	"fmt"

	"gitlab.com/amookia/divarche/users/internal/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

const (
	collectionName = "users"
)

var (
	err error
)

type Repository interface {
	Create(context.Context, entity.UsersModel) error
	Delete(context.Context, string) error
	Update(context.Context, entity.UsersModel) error
	FindByNumber(context.Context, string) entity.UsersModel
}

type repository struct {
	db     *mongo.Database
	logger *zap.Logger
}

func NewRepository(db *mongo.Database, logger *zap.Logger) repository {
	return repository{db: db, logger: logger}
}

func (r repository) Create(ctx context.Context, model entity.UsersModel) error {
	_, err = r.db.Collection(collectionName).InsertOne(ctx, model)
	if err != nil {
		fmt.Println(err)
	}
	return err
}

//delete by number
func (r repository) Delete(ctx context.Context, number string) error {
	_, err = r.db.Collection(collectionName).DeleteOne(ctx, bson.M{"number": number})
	if err != nil {
		fmt.Println(err)
	}
	return err
}

func (r repository) Update(ctx context.Context, model entity.UsersModel) error {
	filter := bson.M{"number": model.PhoneNumber}
	update := bson.M{"$set": model}
	_, err = r.db.Collection(collectionName).
		UpdateOne(ctx, filter, update)
	if err != nil {
		fmt.Println(err)
	}
	return err
}

func (r repository) FindByNumber(ctx context.Context, number string) entity.UsersModel {
	var model entity.UsersModel
	r.db.Collection(collectionName).
		FindOne(ctx, bson.M{"number": number}).Decode(&model)
	return model
}
