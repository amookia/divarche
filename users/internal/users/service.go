package users

import (
	"context"

	"gitlab.com/amookia/divarche/users/internal/entity"
	"go.uber.org/zap"
)

type service struct {
	repo Repository
	log  *zap.Logger
}

func NewUsers(repo Repository, l *zap.Logger) *service {
	return &service{repo: repo, log: l}
}

func (u *service) GetUser(ctx context.Context, gu *GetUserByIdRequest) (*Error, error) {
	u.log.Sugar().Infow("New request has been recieved!")
	return &Error{Error: "AMOOKIA"}, nil
}

func (u *service) CreateUser(ctx context.Context, cu *CreateUserRequest) (*Error, error) {
	u.log.Sugar().Infow("New request has been recieved!")
	re := entity.UsersModel{Name: cu.Name, Last: cu.Last, PhoneNumber: cu.PhoneNumber}
	err := u.repo.Create(ctx, re)
	if err != nil {
		u.log.Sugar().Errorw("Users Service : ", err.Error())
		return &Error{Error: "Duplicate PhoneNumber"}, err
	}
	u.log.Sugar().Info("User Name : ", cu.Name, " Last : ",
		cu.Last, " PhoneNumber : ", cu.PhoneNumber, " created!")
	return &Error{Error: ""}, nil
}

func (u *service) DeleteUser(ctx context.Context, du *DeleteUserRequest) (*Error, error) {
	err := u.repo.Delete(ctx, du.PhoneNumber)
	if err != nil {
		u.log.Sugar().Errorw("Users Service : ", err.Error())
		return &Error{Error: "Not found"}, err
	}
	return &Error{Error: ""}, nil
}

func (u *service) UpdateUser(ctx context.Context, uu *UpdateUserRequest) (*Error, error) {
	re := entity.UsersModel{Last: uu.Last, Name: uu.Name, PhoneNumber: uu.PhoneNumber}
	err := u.repo.Update(ctx, re)
	if err != nil {
		u.log.Sugar().Errorln(err)
	}
	return &Error{Error: ""}, nil
}

func (u *service) GetUserByNumber(ctx context.Context, gu *GetUserByNumberRequest) (*GetUserByNumberResponse, error) {
	model := u.repo.FindByNumber(ctx, gu.PhoneNumber)
	return &GetUserByNumberResponse{
		ID: "", PhoneNumber: model.PhoneNumber, Name: model.Name, Last: model.Last}, nil
}
